package com.example.sfltest

import android.app.Application
import com.example.sfltest.di.component.AppComponent
import com.example.sfltest.di.component.DaggerAppComponent

class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.create()
    }
}