package com.example.sfltest.data.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendingData (
    val bankId: String,
    val name: String,
    val logo: String
) : Parcelable