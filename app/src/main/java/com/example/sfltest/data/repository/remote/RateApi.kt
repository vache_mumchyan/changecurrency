package com.example.sfltest.data.repository.remote

import com.example.sfltest.data.entity.BanksResponse
import com.example.sfltest.data.entity.BranchesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RateApi {

    @GET("ws/mobile/v2/rates.ashx")
    fun getRates(@Query("lang") lang: String): Single<Map<String, BanksResponse>>

    @GET("ws/mobile/v2/branches.ashx")
    fun getBankById(@Query("id") id: String): Single<BranchesResponse>
}  