package com.example.sfltest.data.repository.remote.mapper

import com.example.sfltest.data.entity.Bank
import com.example.sfltest.data.entity.BanksResponse
import com.example.sfltest.utils.BASE_URL
import com.example.sfltest.utils.LOGO_PATH
import io.reactivex.functions.Function


class BanksResponseToBankListMapper : Function<Map<String, BanksResponse>, List<Bank>> {

    override fun apply(map: Map<String, BanksResponse>): List<Bank> {
        val bankList = mutableListOf<Bank>()

        map.entries.forEach {
           val id = it.key
            it.value.run {
                val currency = CurrencyMapToCurrencyListMapper().apply(currencyMap)
                bankList.add(Bank(id, name, date, BASE_URL + LOGO_PATH + logo, currency))
            }
        }

        return bankList
    }
}