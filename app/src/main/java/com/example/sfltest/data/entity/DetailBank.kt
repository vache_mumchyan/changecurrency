package com.example.sfltest.data.entity

class BranchesResponse (
    val date: Double,
    val list: Map<String, Branch>
)


class Branch(
    val head: Int,
    val title: Map<String, String>,
    val address: Map<String, String>,
    val location: Location,
    val contacts: String,
    val workhours: List<WorkDate>
)

data class WorkDate(
    val days: String,
    val hours: String
)

data class Location(
    val lat: Float,
    val lng: Float
)

data class HeadBranch(
    val name : String,
    val title : String,
    val logo: String,
    val address: String,
    val contacts: String,
    val workDay: WorkDate,
    val weekend: WorkDate?,
    val location: Location

)

