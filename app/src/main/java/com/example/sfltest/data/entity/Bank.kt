package com.example.sfltest.data.entity

import com.google.gson.annotations.SerializedName


data class Bank(
    val id: String,
    val name: String,
    val date: Long,
    val logo: String,
    val currencyList: MutableList<Currency>
    )

data class BanksResponse(
    @SerializedName("title") val name: String,
    val date: Long,
    val logo: String,
    @SerializedName("list") val currencyMap: Map<String, Map<String, ExchangeRate>>
)

data class Currency(
    val name: String,
    val exchangeRate: ExchangeRate
)

data class ExchangeRate(
    val buy: Float,
    val sell: Float
)




