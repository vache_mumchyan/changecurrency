package com.example.sfltest.data.repository.remote.mapper

import com.example.sfltest.data.entity.*
import com.example.sfltest.utils.DEFAULT_LANGUAGE
import io.reactivex.functions.BiFunction

class BranchesResponseToHeadBranchMapper : BiFunction<BranchesResponse, SendingData, HeadBranch> {
    private val emptyWorkDate = WorkDate("", "")
    private val emptyLocation = Location(0.0f, 0.0f)

    override fun apply(response: BranchesResponse, sendData: SendingData): HeadBranch {
        response.list.values.forEach { branch ->
            if (branch.head == 1) {
                val weekend = if (branch.workhours.size > 1) branch.workhours[1] else null
                return HeadBranch(
                    sendData.name,
                    branch.title[DEFAULT_LANGUAGE].toString(),
                    sendData.logo, branch.address[DEFAULT_LANGUAGE].toString(), branch.contacts,
                    branch.workhours[0], weekend, branch.location
                )
            }
        }
        return HeadBranch(
            "", "", "", "",
            "", emptyWorkDate, emptyWorkDate, emptyLocation
        )
    }

}