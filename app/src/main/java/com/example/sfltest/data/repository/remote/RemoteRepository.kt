package com.example.sfltest.data.repository.remote

import com.example.sfltest.data.entity.BanksResponse
import com.example.sfltest.data.entity.BranchesResponse
import io.reactivex.Single
import javax.inject.Inject

class RemoteRepository @Inject constructor(private val rateApi: RateApi) {

    fun getRates(lang : String): Single<Map<String, BanksResponse>> = rateApi.getRates(lang)

    fun getBankById(id: String): Single<BranchesResponse> = rateApi.getBankById(id)


}