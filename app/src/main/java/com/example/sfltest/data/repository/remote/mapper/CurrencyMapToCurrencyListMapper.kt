package com.example.sfltest.data.repository.remote.mapper

import com.example.sfltest.data.entity.Currency
import com.example.sfltest.data.entity.ExchangeRate
import io.reactivex.functions.Function


class CurrencyMapToCurrencyListMapper : Function<Map<String, Map<String, ExchangeRate>>, MutableList<Currency>> {

    override fun apply(input: Map<String, Map<String, ExchangeRate>>): MutableList<Currency> {
        val currencyList = mutableListOf<Currency>()

        input.entries.forEach {
            val name = it.key
            val exchangeRate = it.value.values.iterator().next()
            currencyList.add(Currency(name, exchangeRate))
        }

        return currencyList
    }
}