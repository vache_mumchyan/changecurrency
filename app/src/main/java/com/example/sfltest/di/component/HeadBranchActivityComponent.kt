package com.example.sfltest.di.component

import com.example.sfltest.di.module.MainViewModelFactoryModule
import com.example.sfltest.di.scope.DetailScope
import com.example.sfltest.ui.detail.HeadBranchActivity
import dagger.Component


@DetailScope
@Component(
    dependencies = [AppComponent::class], modules = [MainViewModelFactoryModule::class]
)
interface HeadBranchActivityComponent {

    fun inject(headBranchActivity: HeadBranchActivity)

}