package com.example.sfltest.di.module

import androidx.lifecycle.ViewModelProvider
import com.example.sfltest.di.ViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
abstract class MainViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelModelFactory(modelProviderProviderFactory: ViewModelProviderFactory)
            : ViewModelProvider.Factory
}