package com.example.sfltest.di.component

import com.example.sfltest.di.module.MainViewModelFactoryModule
import com.example.sfltest.di.scope.MainScope
import com.example.sfltest.ui.main.MainActivity
import dagger.Component

@MainScope
@Component(
    dependencies = [AppComponent::class], modules = [MainViewModelFactoryModule::class]
)
interface MainActivityComponent {

    fun inject(mainActivity: MainActivity)

}