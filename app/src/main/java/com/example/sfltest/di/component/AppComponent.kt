package com.example.sfltest.di.component

import com.example.sfltest.di.module.NetworkModule
import com.example.sfltest.data.repository.remote.RemoteRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component( modules = [
    NetworkModule::class
] )
interface AppComponent {

    fun getRemoteRepository(): RemoteRepository
}