package com.example.sfltest.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.essentials.moviestest.ui.main.MainViewModel
import com.example.sfltest.data.repository.remote.RemoteRepository
import com.example.sfltest.ui.detail.HeadBranchDetailViewModel
import javax.inject.Inject

class ViewModelProviderFactory @Inject constructor(
    private val repository: RemoteRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return when {
            modelClass.isAssignableFrom(MainViewModel::class.java) -> {
                MainViewModel(
                    repository
                ) as T
            }
            modelClass.isAssignableFrom(HeadBranchDetailViewModel::class.java) -> {
                HeadBranchDetailViewModel(
                    repository
                )as T
            }
            else -> {
                throw IllegalArgumentException("ViewModel Not Found")
            }
        }
    }

}