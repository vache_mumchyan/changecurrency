package com.example.sfltest.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sfltest.data.entity.HeadBranch
import com.example.sfltest.data.entity.SendingData
import com.example.sfltest.data.entity.WorkDate
import com.example.sfltest.data.repository.remote.RemoteRepository
import com.example.sfltest.data.repository.remote.mapper.BranchesResponseToHeadBranchMapper
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HeadBranchDetailViewModel @Inject constructor(
    private val repository: RemoteRepository
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val headBranch = MutableLiveData<HeadBranch>()

    fun init(sendingData: SendingData?) {
        sendingData?.let {
            repository.getBankById(sendingData.bankId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .zipWith(Single.just(sendingData), BranchesResponseToHeadBranchMapper())
                .subscribe({
                    headBranch.value = it
                }, {

                })
                .let {
                    compositeDisposable.add(it)
                }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}