package com.example.essentials.moviestest.ui.main

import android.view.View
import android.widget.AdapterView
import androidx.databinding.adapters.AdapterViewBindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sfltest.data.entity.Bank
import com.example.sfltest.data.entity.Currency
import com.example.sfltest.data.repository.remote.RemoteRepository
import com.example.sfltest.data.repository.remote.mapper.BanksResponseToBankListMapper
import com.example.sfltest.utils.DEFAULT_LANGUAGE
import com.example.sfltest.utils.ERROR_MESSAGE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val repository: RemoteRepository
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val allBankList = mutableListOf<Bank>()
    var currentBankList = MutableLiveData<List<Bank>>()
    var currencyNameList = MutableLiveData<List<String>>()
    var showLoading = MutableLiveData<Boolean>()
    var errorMessage = MutableLiveData<String>()

    val onSpinnerItemSelected = object : OnSpinnerItemSelected {
        override fun onItemClick(value: String) {
            currentBankList.value = getBankLIstByCurrency(value)
        }
    }

    fun init() {
        showLoading.value = true
        repository.getRates(DEFAULT_LANGUAGE)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map(BanksResponseToBankListMapper())
            .subscribe({
                allBankList.addAll(it)
                currencyNameList.value = getCurrencyNameList(it)
                showLoading.value = false

            }, {
                showLoading.value = false
                errorMessage.value = ERROR_MESSAGE
            })
            .let {
                compositeDisposable.add(it)
            }
    }

    private fun getBankLIstByCurrency(currencyName: String): List<Bank> {
        val bankList = mutableListOf<Bank>()
        allBankList.forEach { bank ->
            bank.currencyList.forEach { currency ->
                if (currency.name == currencyName) {
                    bank.run {
                        bankList.add(Bank(id, name, date, logo, mutableListOf(currency)))
                    }
                }
            }
        }
        return bankList
    }

    private fun getCurrencyNameList(listBank: List<Bank>): List<String> {
        val currencySet = mutableSetOf<String>()
        listBank.forEach { bank ->
            bank.currencyList.forEach { currency ->
                currencySet.add(currency.name)
            }
        }
        return currencySet.toList()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    interface OnSpinnerItemSelected {
        fun onItemClick(value: String)
    }
}

