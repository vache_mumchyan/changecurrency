package com.example.sfltest.ui

import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.example.essentials.moviestest.ui.main.MainViewModel


object BindingAdapter {

    @BindingAdapter("onSpinnerItemSelected")
    @JvmStatic
    fun onSpinnerItemSelected(
        view: AppCompatSpinner,
        onItemSelectedListener: MainViewModel.OnSpinnerItemSelected
    ) {
        view.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                view as AppCompatTextView
                onItemSelectedListener.onItemClick(view.text.toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }
}