package com.example.sfltest.ui.detail

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.bumptech.glide.Glide
import com.example.sfltest.App
import com.example.sfltest.R
import com.example.sfltest.data.entity.SendingData
import com.example.sfltest.di.ViewModelProviderFactory
import com.example.sfltest.di.component.DaggerHeadBranchActivityComponent
import com.example.sfltest.utils.*
import kotlinx.android.synthetic.main.activity_head_branch.*
import javax.inject.Inject


class HeadBranchActivity : AppCompatActivity() {

    @Inject
    lateinit var detailViewModelProvider: ViewModelProviderFactory
    private lateinit var viewModelHeadBranch: HeadBranchDetailViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_head_branch)

        val sendingData = intent.getParcelableExtra<SendingData>(KEY_SENDING)

        DaggerHeadBranchActivityComponent.builder()
            .appComponent((applicationContext as App).appComponent)
            .build()
            .inject(this)

        viewModelHeadBranch = ViewModelProvider(
            this,
            detailViewModelProvider
        ).get(HeadBranchDetailViewModel::class.java)

        viewModelHeadBranch.init(sendingData)

        viewModelHeadBranch.headBranch.observe(this, Observer { headBranch ->

            Glide.with(this)
                .load(headBranch.logo)
                .into(imgDetailLogo)

            headBranch.run {
                tvTitle.text = name
                tvCity.text = title
                tvAddress.text = address
                tvPhoneNumber.text = contacts
                tvPhoneNumber.paintFlags = Paint.UNDERLINE_TEXT_FLAG
                tvDaysAndHours.text = resources.getString(R.string.monday_friday) + workDay.hours
                weekend?.let {
                    saturdayHours.text = SATURDAY + "  " + it.hours
                }

            }
        })
    }
}
