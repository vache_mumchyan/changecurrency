package com.example.sfltest.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sfltest.R
import com.example.sfltest.data.entity.Bank


class BanksAdapter(private val itemClickListener: ItemClickListener) :
    RecyclerView.Adapter<BanksAdapter.BankViewHolder>() {

    var listBanks = listOf<Bank>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_bank, parent, false)

        return BankViewHolder(view)
    }

    override fun getItemCount(): Int = listBanks.size

    override fun onBindViewHolder(holder: BankViewHolder, position: Int) {
        holder.bind(listBanks[position])
    }

    inner class BankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvTitle: TextView = itemView.findViewById(R.id.tvBankName)
        private val tvBuy: TextView = itemView.findViewById(R.id.tvBuy)
        private val tvSell: TextView = itemView.findViewById(R.id.tvSell)
        private val imgLogo: ImageView = itemView.findViewById(R.id.imgLogo)

        fun bind(bank: Bank) {
            tvTitle.text = bank.name
            tvBuy.text = bank.currencyList[0].exchangeRate.buy.toString()
            tvSell.text = bank.currencyList[0].exchangeRate.sell.toString()

            Glide.with(itemView)
                .load(bank.logo)
                .into(imgLogo)

            itemView.setOnClickListener {
                itemClickListener.onBankClickListener(
                    listBanks[adapterPosition].id, bank.logo,
                    listBanks[adapterPosition].name
                )
            }
        }
    }

    interface ItemClickListener {
        fun onBankClickListener(
            bankId: String,
            logoPath: String,
            bankName: String
        )
    }
}