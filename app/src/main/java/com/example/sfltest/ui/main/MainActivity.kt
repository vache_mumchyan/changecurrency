package com.example.sfltest.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.essentials.moviestest.ui.main.MainViewModel
import com.example.sfltest.App
import com.example.sfltest.R
import com.example.sfltest.data.entity.SendingData
import com.example.sfltest.databinding.ActivityMainBinding
import com.example.sfltest.di.ViewModelProviderFactory
import com.example.sfltest.di.component.DaggerMainActivityComponent
import com.example.sfltest.ui.detail.HeadBranchActivity
import com.example.sfltest.utils.KEY_SENDING
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), BanksAdapter.ItemClickListener {

    @Inject
    lateinit var viewModelProvider: ViewModelProviderFactory
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var bankAdapter: BanksAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        DaggerMainActivityComponent.builder()
            .appComponent((applicationContext as App).appComponent)
            .build()
            .inject(this)


        viewModel = ViewModelProvider(this, viewModelProvider)[MainViewModel::class.java]
        binding.mainViewModel = viewModel

        viewModel.init()

        rateRecycler.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        bankAdapter = BanksAdapter(this)
        rateRecycler.adapter = bankAdapter

        viewModel.currencyNameList.observe(this, Observer {
            val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, it)
            currencySpinner.adapter = spinnerAdapter
        })

        viewModel.currentBankList.observe(this, Observer {
            bankAdapter.listBanks = it
        })

        viewModel.showLoading.observe(this, Observer {
            progressBar.visibility = if (it) View.VISIBLE else View.GONE
        })

        viewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

    }

    override fun onBankClickListener(bankId: String, logoPath: String, bankName: String) {
        val intent = Intent(this, HeadBranchActivity::class.java)
        intent.putExtra(KEY_SENDING, SendingData(bankId, bankName, logoPath))
        startActivity(intent)
    }
}
